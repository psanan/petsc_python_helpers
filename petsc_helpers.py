""" Helper functions for working with PETSc output in (I)Python """

import numpy as np
from scipy import sparse

import PetscBinaryIO as pio  # $PETSC_DIR/lib/petsc/bin should be in PYTHONPATH

def pload(filename):
    """ Load a single Vec or Mat from a PETSc binary file,
        returning a NumPy array or a SciPy sparse CSR matrix """
    io = pio.PetscBinaryIO()
    with open(filename) as binary_file:
        objecttype = io.readObjectType(binary_file)
        if objecttype == 'Mat':
            matrix = io.readMat(binary_file)
            shape = matrix[0]
            data = matrix[1][2]
            row_ind = matrix[1][0]
            col_ind = matrix[1][1]
            result = sparse.csr_matrix((data, col_ind, row_ind), shape=shape)
            result.maxprint = np.inf # Don't truncate on print()
        elif objecttype == 'Vec':
            result = io.readVec(binary_file)
        else:
            raise Exception('Cannot read a Mat or Vec from ' + filename)
    return result
