## Helpers for working with PETSc and Python

To use, make sure that `$PETSC_DIR/lib/petsc/bin` and this directory are in
`PYTHONPATH`.

Then, you should be able to import this module and use the functions, e.g. to
load a matrix from a PETSc binary output file and plot it. You can do the
import when you start up IPython with the setup file included here, e.g.

    ipython --matplotlib -i $HOME/code/petsc_python_helpers/ipython_setup.py

    A = pload('binaryoutput')
    plt.matshow(A.toarray())

The main purpose is for quick debugging and analysis. An often-handy trick is
to run your program with gdb, stop somewhere where you want to see a Mat or Vec,
and execute e.g. `call MatView(A,PETSC_VIEWER_BINARY_WORLD)`, and then load
the resulting `binaryoutput` file as above.
