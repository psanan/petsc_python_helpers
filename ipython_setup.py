""" A setup script meant to be run with ipython --matplotlib -i """

import sys
import numpy
import matplotlib
from matplotlib import pyplot
np = numpy
plt = pyplot

from petsc_helpers import pload

print("Running", __file__)

# Turn off NumPy's line wrapping
np.set_printoptions(linewidth=np.inf)

# Turn off NumPy's truncation
np.set_printoptions(threshold=sys.maxsize)
